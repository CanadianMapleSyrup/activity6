package activity6;

public abstract class VegetarianSandwich implements ISandwich{
    private String filling =" ";

    @Override
    public void addFilling(String topping)
    {
        String[] meats = {"chicken","beef","fish","meat","pork"};
        for (String meat : meats)
        {
            if (topping.toLowerCase().equals(meat))
            {
                throw new IllegalArgumentException();
            }
        }
        this.filling += topping + " ";
    }

    @Override
    public boolean isVegetarian()
    {
        return true;
    }

    public boolean isVegan()
    {
        if (this.filling.toLowerCase().contains("cheese") || this.filling.toLowerCase().contains("egg"))
        {
            return false;
        }
        return true;
    }

    public abstract String getProtein();

    public String getFilling()
    {
        return this.filling;
    }
}
