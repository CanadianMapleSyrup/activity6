package activity6;

public class Application {
    public static void main(String[] args) {
        BagelSandwich b1 = new BagelSandwich();
        System.out.println(b1.getFilling());
        b1.addFilling("Cream cheese");
        System.out.println(b1.getFilling());
        b1.addFilling("Salmon spread");
        System.out.println(b1.getFilling());

        ISandwich s1 = new TofuSandwich();
        System.out.println(s1 instanceof ISandwich);
        System.out.println(s1 instanceof VegetarianSandwich);
        //s1.addFilling("chicken"); Throws Exception
        VegetarianSandwich s2 = ((TofuSandwich)s1);
        System.out.println(s2.isVegan());
        VegetarianSandwich s3 = new ChickenSandwich();
        System.out.println(s3.isVegetarian());
    }
}
