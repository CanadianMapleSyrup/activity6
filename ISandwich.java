package activity6;

public interface ISandwich {

    String getFilling();

    void addFilling(String topping);

    boolean isVegetarian();
}